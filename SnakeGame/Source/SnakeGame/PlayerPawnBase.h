// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class APoison;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
		APoison* PoisonActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<APoison> PoisonActorClass;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	float MinY = -330.f; float MaxY = 1300.f;
	float MinX = -330.f; float MaxX = 1300.f;
	float SpawnZ = 30.f;

	/*float MinPY = -330.f; float MaxPY = 1300.f;
	float MinPX = -330.f; float MaxPX = 1300.f;
	float SpawnPZ = 30.f;*/

	float StepDeley = 8.f;
	float BufferTime = 0;

	float StepDeleyPoison = 20.f;
	float BufferTimePoison = 0;
	
	void AddRandomFood();
	void AddRandomPoison();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
};
